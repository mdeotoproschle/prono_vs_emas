# prono_vs_emas


# Comparacion de Temperatura pronosticada y observaciones meteorológicas regionales


## Descripción

En este caso utilizaremos el código de descarga de observaciones meteorológicas con muestreo sub-horario proveniente de las estaciones meteorológicas automáticas (EMAs) de [Wunderground](https://www.wunderground.com/). El código es del tipo de web scraping y se encuentra alojado y detallado en el repositorio [git](https://gitlab.com/mdeotoproschle/scrap_wunder). A su vez, utilizaremos la información proveniente de los pronósticos globales del ECMWF y GFS y de los regionales como el WRF-SMN para compararlos con las EMAs. En este caso de ejemplo, sólo compararemos la variable temperatura a 2 m.  


## Pasos

El codigo es secuencial y esta explicado por bloques:

1. Los primeros bloques importan las librerias que ya se encuentran en la notebook virtual de colab y que serán requeridas en la ejecución.
2. Los bloques siguientes descargan las librerías no instaladas por la notebook virtual, una vez descargadas las importan.
3. Se importa desde el repositorio [git](https://gitlab.com/mdeotoproschle/scrap_wunder) la función de web scraping.
4. Se descargan los pronósticos de los modelos, en algunos casos se utiliza paralelización mediante la construcción de funciones de descarga. Existe un limitante de memoria virtual para realizar las descargas de los pronósticos, y en algunos casos se opta por descarga secuencial. La descarga paralelizada descarga varios archivos en paralelo, mientras que la descarga en secuencia descarga uno despues del otro. Este ultimo es menos eficiente temporalmente, sin embargo consume menos memoria que la paralelizacion.
5. Una vez descargados los archivos de pronosticos, se cargan y se concatenan.
6. Se realizan el grafico de temperatura de las EMAs y los pronosticos.


### Características de las observaciones de las EMAs

Se utilizan las EMAs locales de la ciudad de San Carlos de Bariloche. La lista de EMAs se incorpora en el código manualmente en formato de lista. Actualmente, se cuenta con 9 estaciones, aunque se pueden agregar de zonas un poco más [lejanas](https://www.wunderground.com/wundermap?lat=-41.133&lon=-71.31). Si se desea utilizar otra región de estudio, se recomienda mantener la estructura de la información. Por otra parte, por una cuestión de eficiencia temporal, se aconseja no descargar información por un periodo mayor a 3 días, a menos que se cuente con suficiente memoria como para paralelizar los procesos. La información meteorológica descargada se encuentra en resolución temporal de 5-minutos dentro de la base de datos de Weather Underground.


### Caracteristicas de los modelos numéricos

Los modelos numericos que realizan los pronosticos del tiempo son distintos ya que presentan algunas de las siguientes diferencias:

- El [dominio](https://www.noaa.gov/jetstream/upper-air-charts/weather-models), si es global o regional
- La resolución espacial y temporal. Para observar estas diferencias en la resolución se recomienda ejecutar el codigo denominado [resolucion](tito)
- Las [parametrizaciones](https://en.wikipedia.org/wiki/Parametrization_(climate_modeling)) de los procesos físicos
- Los esquemas de [asimilacion](https://www.ecmwf.int/en/research/data-assimilation) de los datos.

Para obtener mayor información de los modelos se recomienda leer los siguientes enlaces asociados
    - **[ECMWF](https://www.ecmwf.int/en/forecasts/documentation-and-support/medium-range-forecasts)**
    - **[GFS](https://www.emc.ncep.noaa.gov/emc/pages/numerical_forecast_systems/gfs.php)**
    - **[WRF](https://odp-aws-smn.github.io/documentation_wrf_det/Informacion_general/)**

### Requerimientos

    * Generarse una cuenta en [NCAR](https://rda.ucar.edu/login/register/) para descargar los pronósticos del modelo GFS
    * Manejo de Google [Colab](https://colab.research.google.com/)

### Ejecución

1. Bajar el archivo **.ipynb** del git a la computadora personal (para bajarlo, apretar el botón derecho del mouse sobre el link, y elegir la opción para bajar el archivo)

2. Dirijirse a *[Colab](https://colab.research.google.com)*

3. En Colab, dirijirse a la pestaña **Subir (Upload)**, apretar y subir el archivo **.ipynb**

4. Ejecutarlo todo de una vez en la solapa Runtime >> Run All, ó ejecurtalo paso por paso mediante el ícono de "play" sobre el margen izquierdo de cada celda.

## ATENCION
A continuacion se presentan algunas configuraciones/condiciones que se deberan tener en cuenta a la hora de ejecutar el modelo.
Particularmente relacionadas a la temporalidad.

1. Los pronosticos del modelo Global del ECMWF se otorgan de forma publica sólo hasta dos días previos a la fecha actual. Para acceder a pronostico más antiguos se debe contar con [credenciales](https://confluence.ecmwf.int/display/WEBAPI/Access+MARS) especiales. Hay beneficios siendo de instituciones de investigación, caso contrario los datos más antiguos son arancelados.

2. Actualmente, la descarga de los pronsoticos del GFS son globales, es decir no hay posibilidad de cortar la región de estudio. Como desventaja de esto, los archivos suelen ser muy pesados (hasta 0.5 GB) y demoran un buen rato en descargarse. Podría implementarse la funcion de paralelizacion en este caso.

3. Los archivos de pronósticos del WRF-SMN son sin costo y presentan una resolución horaria. Sin embargo, al ser un modelo regional y de alta resolución, su alcance es de 72 horas. Por lo que para realizar la comparación entre pronosticos y observaciones de EMAs tendremos dos limitantes:

              * Fechas con antelación a 2 días estan fuera del programa de obtención pública de datos meteorológicos del ECMWF
              * Los pronósticos del WRF-SMN sólo se producen con alta reolución hasta 72 horas después de la fecha actual (ciclo de 00).

# Agradecimientos

Al esfuerzo del Servicio Meteorológico NAcional por disponer de forma no arancelada los pronósticos del modelo regional WRF-SMN en Amazon Web Service
